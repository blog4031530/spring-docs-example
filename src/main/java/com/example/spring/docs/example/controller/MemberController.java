package com.example.spring.docs.example.controller;


import com.example.spring.docs.example.dto.MemberDto;
import com.example.spring.docs.example.service.MemberService;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class MemberController {
	private final MemberService memberService;

	@GetMapping(path = "/members/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MemberDto> getMember(@PathVariable String id) {
		log.info("called getMember");
		MemberDto member = memberService.getMember(id);
		return ResponseEntity.ok(member);
	}

	@PostMapping(path = "/members", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MemberDto> createMember(@RequestBody MemberDto memberDto) {
		log.info("called createMember");
		MemberDto member = memberService.createMember(memberDto);
		return ResponseEntity.ok(member);
	}

	@GetMapping(path = "/members-param", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<MemberDto> createMemberParam(
		@RequestHeader(name = "x-custom-header") String customHeader,
		@RequestParam(name = "firstName") @NotBlank String firstName,
		@RequestParam(name = "lastName") @NotBlank String lastName,
		@RequestParam(name = "age") @Min(1) @Max(100) Integer age,
		@RequestParam(name = "hobby", required = false) String hobby) {
		log.info("called createMember parameter, header: {}", customHeader);
		MemberDto member = memberService.createMember(firstName, lastName, age, hobby);
		return ResponseEntity.ok(member);
	}
}
