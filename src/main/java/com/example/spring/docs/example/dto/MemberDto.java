package com.example.spring.docs.example.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record MemberDto(
	String id,
	@NotBlank
	String firstName,

	@NotBlank
	String lastName,

	@Min(1) @Max(100)
	Integer age,
	String hobby) {

	public MemberDto withId(String id) {
		return MemberDto.builder()
			.id(id)
			.firstName(firstName)
			.lastName(lastName)
			.age(age)
			.hobby(hobby)
			.build();
	}
}
