package com.example.spring.docs.example.service;

import com.example.spring.docs.example.dto.MemberDto;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MemberService {

	public MemberDto getMember(String id) {
		return MemberDto.builder()
			.id(id)
			.firstName("dummy")
			.lastName("name")
			.hobby("study").build();
	}

	public MemberDto createMember(MemberDto memberDto) {
		return memberDto.withId(UUID.randomUUID().toString());
	}

	public MemberDto createMember(String firstName, String lastName, Integer age, String hobby) {
		return MemberDto.builder()
			.id(UUID.randomUUID().toString())
			.firstName(firstName)
			.lastName(lastName)
			.age(age)
			.hobby(hobby)
			.build();
	}
}
