package com.example.spring.docs.example.controller;

import com.example.spring.docs.example.BaseDocumentTest;
import com.example.spring.docs.example.dto.MemberDto;
import com.example.spring.docs.example.service.MemberService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.JsonFieldType;

import java.util.UUID;

import static com.epages.restdocs.apispec.MockMvcRestDocumentationWrapper.document;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MemberControllerTest extends BaseDocumentTest {
	@MockBean
	MemberService memberService;

	@DisplayName("멤버 정보를 가져오는 테스트")
	@Test
	void getMember() throws Exception {
		MemberDto memberDto = MemberDto.builder()
			.id(UUID.randomUUID().toString())
			.firstName("firstname")
			.lastName("lastname")
			.age(20)
			.hobby("study")
			.build();

		given(memberService.getMember(anyString())).willReturn(memberDto);

		this.mockMvc.perform(
				RestDocumentationRequestBuilders.get("/members/{id}", UUID.randomUUID())
				.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andDo(
				document(snippetPath,
					"아이디 기반 멤버 정보를 조회하는 API",
					//아래 부터는 spring REST docs snippets 를 정의한다
					//path parameter 에 대한 문서 정의
					pathParameters(
						parameterWithName("id").description("멤버 아이디")
					),
					//응답에 대한 문서 정의
					responseFields(
						fieldWithPath("id").type(JsonFieldType.STRING).description("멤버 아이디"),
						fieldWithPath("firstName").type(JsonFieldType.STRING).description("성"),
						fieldWithPath("lastName").type(JsonFieldType.STRING).description("이름"),
						fieldWithPath("age").type(JsonFieldType.NUMBER).description("나이"),
						fieldWithPath("hobby").type(JsonFieldType.STRING).description("취미").optional()
					)
				)
			);
	}

	@DisplayName("멤버를 생성하는 테스트")
	@Test
	void createMember() throws Exception {
		MemberDto memberDto = MemberDto.builder()
			.firstName("firstname")
			.lastName("lastname")
			.age(20)
			.build();

		MemberDto responseDto = memberDto.withId(UUID.randomUUID().toString());

		given(memberService.createMember(any())).willReturn(responseDto);

		//MemberDto 의 validation 이 지정된 제약사항을 구문 분석한다.
		ConstraintDescriptions constraintDescriptions = new ConstraintDescriptions(MemberDto.class);

		this.mockMvc.perform(
				RestDocumentationRequestBuilders.post("/members")
					.content(createJson(memberDto))
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andDo(
				document(snippetPath,
					"멤버 정보를 생성하는 API",
					//요청에 대한 문서 정의
					requestFields(
						//요청 body payload 에서 member dto 정의의 id 필드 정보는 없으므로 ignored 를 호출한다.
						fieldWithPath("id").ignored(),
						fieldWithPath("firstName")
							.type(JsonFieldType.STRING)
							.description("멤버 성")
							//MemberDto 의 firstName 필드의 제약사항을 문서화
							.attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("firstName"))),
						fieldWithPath("lastName")
							.type(JsonFieldType.STRING)
							.description("멤버 이름")
							//MemberDto 의 lastName 필드의 제약사항을 문서화
							.attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("lastName"))),
						fieldWithPath("age")
							.type(JsonFieldType.NUMBER)
							.description("멤버 나이")
							//MemberDto 의 age 필드의 제약사항을 문서화
							.attributes(
								key("constraints")
									.value(String.join(" + \n", constraintDescriptions.descriptionsForProperty("age")))),
						fieldWithPath("hobby").type(JsonFieldType.STRING).description("멤버 취미").optional()
					),
					//응답에 대한 문서 정의
					responseFields(
						fieldWithPath("id").type(JsonFieldType.STRING).description("멤버 아이디"),
						fieldWithPath("firstName").type(JsonFieldType.STRING).description("성"),
						fieldWithPath("lastName").type(JsonFieldType.STRING).description("이름"),
						fieldWithPath("age").type(JsonFieldType.NUMBER).description("나이"),
						fieldWithPath("hobby").type(JsonFieldType.STRING).description("취미").optional()
					)
				)
			);
	}

	@DisplayName("파라미터 기반 멤버를 생성하는 테스트")
	@Test
	void createMemberParam() throws Exception {
		String firstName = "firstname";
		String lastName = "lastname";
		Integer age = 20;
		MemberDto memberDto = MemberDto.builder()
			.id(UUID.randomUUID().toString())
			.firstName(firstName)
			.lastName(lastName)
			.age(age)
			.build();

		//request 가 파라미터 방식이라도 파라미터 필드가 동일하다면 MemberDto 의 validation 을 그대로 활용할 수 있다.
		//MemberDto 의 validation 정보를 구문분석하여 문서화에 사용될 뿐임을 알 수 있다.
		ConstraintDescriptions constraintDescriptions = new ConstraintDescriptions(MemberDto.class);

		//3번째 필드인 hobby 는 optional 이기 때문에 anyString 이 아닌 any() 로 선언해야 한다
		given(memberService.createMember(
				anyString(),
				anyString(),
				anyInt(),
				any()
			)
		).willReturn(memberDto);

		this.mockMvc.perform(
				RestDocumentationRequestBuilders.get("/members-param")
					.queryParam("firstName", firstName)
					.queryParam("lastName", lastName)
					.queryParam("age", age.toString())
					.header("x-custom-header", "custom header")
					.accept(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andDo(
				document(snippetPath,
					"파라미터 기반 입력으로 멤버를 생성하는 API",
					//요청 헤더 에 대한 문서 정의
					requestHeaders(
						headerWithName("x-custom-header").description("커스텀 헤더 정보")
					),
					//요청 파라미터에 대한 문서 정의
					queryParameters(
						parameterWithName("firstName")
							.description("멤버 이름 성")
							.attributes(
								key("constraints").value(constraintDescriptions.descriptionsForProperty("firstName"))),
						parameterWithName("lastName")
							.description("멤버 이름")
							.attributes(key("constraints").value(constraintDescriptions.descriptionsForProperty("lastName"))),
						parameterWithName("age")
							.description("멤버 나이")
							.attributes(
								key("constraints")
									.value(String.join(" + \n", constraintDescriptions.descriptionsForProperty("age")))),
						parameterWithName("hobby")
							.description("멤버 취미")
							.optional()
					),
					//응답에 대한 문서 정의
					responseFields(
						fieldWithPath("id").type(JsonFieldType.STRING).description("멤버 아이디"),
						fieldWithPath("firstName").type(JsonFieldType.STRING).description("성"),
						fieldWithPath("lastName").type(JsonFieldType.STRING).description("이름"),
						fieldWithPath("age").type(JsonFieldType.NUMBER).description("나이"),
						fieldWithPath("hobby").type(JsonFieldType.STRING).description("취미").optional()
					)
				)
			);
	}
}